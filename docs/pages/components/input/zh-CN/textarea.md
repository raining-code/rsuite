### 多行输入框

<!--start-code-->

```js
const instance = <Input componentClass="textarea" rows={3} placeholder="Textarea" />;

ReactDOM.render(instance);
```

<!--end-code-->
