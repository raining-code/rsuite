import React from 'react';
import './App.css';
import Button from 'rsuite/lib/Button';
import 'rsuite/lib/Button/styles/themes/default';

function App() {
  return (
    <div className="App">
      <Button appearance="primary"> Hello world </Button>
    </div>
  );
}

export default App;
